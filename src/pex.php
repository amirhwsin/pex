<?php

namespace PhpExcelExport;

class Pex {
    
    protected $filename = null;
    public $rows = 0;
    protected $insertedRows = 0;

    protected $_rows = "";
    protected $_columns = "";

    /**
     * Force browser to download excel file.
     * 
     * @var string
     */
    public function download($filename) {
        $this->filename = $filename;
        $handle = fopen($this->filename . ".xls", "w");
        fwrite($handle, $this->_columns . $this->_rows);
        fclose($handle);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename='.basename($this->filename . '.xls'));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($this->filename . '.xls'));
        readfile($this->filename . '.xls');
        unlink($this->filename . '.xls');
    }

    /**
     * Checking is loop free to run or not
     * 
     */
    public function not_the_end() {
        return ($this->insertedRows < $this->rows)?true:false;
    }

    /**
     * Add new row to excel
     * 
     * @var array
     */
    public function addRow($row) {
        $ro = "";
        foreach ($row as $k => $r) {
            if (count($row) - 1 == $k) {
                $ro .= $r . "\n";
            }else {
                $ro .= $r . ",";
            }
        }
        $this->_rows .= $ro;
        unset($ro);
        $this->insertedRows++;
    }

    /**
     * Add multiple rows to excel
     * 
     * @var array
     */
    public function addRows($rows) {
        foreach ($rows as $row) {
            $this->addRow($row);
        }
    }

    /**
     * Add columns to excel
     * 
     * @var array
     */
    public function addColumn($column) {
        $col = "";
        foreach ($column as $k => $c) {
            if (count($column) - 1 == $k) {
                $col .= $c . "\n";
            }else {
                $col .= $c . ",";
            }
        }
        $this->_columns .= $col;
        unset($col);
    }

    /**
     * Creates excel in one function
     * First index is columns and rest of the indexes are rows
     * 
     * @var array infinite
     */
    public function create() {
        for($i = 0; $i <= func_num_args() - 1; $i++) {
            if ($i == 0) {
                $this->addColumn(func_get_arg($i));
                continue;
            }
            $this->addRow(func_get_arg($i));
        }
    }
}